<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$app->get('/', function () use ($app) {
    // return "#semanticHashtags API v0.0.1";
    $hashtags = [];
	return view('home', ['hashtags' => $hashtags]);
});

// Api request route:
$app->post('apiV1', ['as' => 'ApiRoute', 'uses' => 'Apiv1Request@new']);
