<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Validator;
use Goutte\Client;

class Apiv1Request extends Controller
{
    private $hashtags;
    private $currentIndex;
    private $justHashtags;
    private $englishStopwords;
    private $greekStopwords;
    private $processingsLimit;
    private $processings;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    
    }

    /**
     * Handle the new request
     * 
     * @param  String
     * @return JSON String
     */
    public function new(Request $request){

        // initialize hashtags container
        $this->hashtags = [];
        $this->justHashtags = [];

        // set stopwords class variables
        $this->initializeStopwords();

        // initialize current index for the hashtags array
        $this->currentIndex = 0;

        // set a processings limit
        $this->processingsLimit = 100;
        $this->processings = 0;

        /**
         * Validate Request
         * ================
        **/

        $validator = Validator::make($request->all(), [
            'url' => 'required|active_url'
        ]);

        if($validator->fails()){
            return response()->json(['error' => 'Invalid Url']);
        }

        // initialize client
        $client = new Client();

        // start scraping
        $crawler = $client->request('GET', $request->url);

        /*
         * Get site title
         * --------------
         */
        $siteTitle = $crawler->filter('title')->each(function ($node) {
            return ($node->text());
        });

        // push title in hashtags container
        $siteTitle = array_first($siteTitle);
        if(strlen($siteTitle) > 0){
            $siteTitleWords = $this->removeStopwordsFromString($siteTitle);
            $siteTitle = "";
            $max = 2; $c = 0;
            foreach ($siteTitleWords as $word) {
                if($c == $max) break;
                $siteTitle .= str_replace("#", "", $word) . " ";
                $c++;
            }
            $siteTitle = rtrim($siteTitle, " ");
            $this->hashtags[$this->currentIndex]['tag'] = $this->hashify($siteTitle);
            $this->hashtags[$this->currentIndex]['weight'] = 50; // --
            $this->currentIndex++;
        }
        
        $this->processings++;

        /*
         * Read site meta (basically I want to get only the description and keywords)
         * --------------------------------------------------------------------------
         */
        $descriptionWords = [];
        $check = $crawler->filterXpath('//meta[@name="description"]')->count();
        if($check > 0){
            $description = ($crawler->filterXpath('//meta[@name="description"]')->attr('content'));
            // remove stopwords:
            $descriptionWords = $this->removeStopwordsFromString($description);
        }
        $keywords = [];
        $check = $crawler->filterXpath('//meta[@name="keywords"]')->count();
        if($check > 0){
            $keywords = ($crawler->filterXpath('//meta[@name="keywords"]')->attr('content'));
            // remove stopwords:
            $keywords = $this->removeStopwordsFromString($keywords);
        }

        // now that we splitted up good & evil, push the good hashtags in hashtags container
        if(count($descriptionWords)){
            foreach($descriptionWords as $word){
                if(strlen($word) > 1){
                    $this->hashtags[$this->currentIndex]['tag'] = $word;
                    $this->hashtags[$this->currentIndex]['weight'] = 4; // --
                    $this->currentIndex++;
                }
            }
        }
        if(count($keywords)){
            foreach($keywords as $word){
                if(strlen($word) > 1){
                    $this->hashtags[$this->currentIndex]['tag'] = $word;
                    $this->hashtags[$this->currentIndex]['weight'] = 1; // --
                    $this->currentIndex++;
                }
            }
        }

        $this->processings += 2;

        /*
         * Read headings, alt on images, links & labels(removed)
         * --------------------------------------------------------------------------
         */
        $elementsToRead = ['h1', 'h2', 'h3', 'img', 'a'];
        foreach($elementsToRead as $element){
            $filteredElements = $crawler->filter($element)->each(function ($node, $element) {
                // exception for img, on image we just want the alt attr
                if($element == 'img'){
                    // Now lets read some alt attributes from images and who knows, we may find something cool
                    $cleanElementsArray = $this->removeStopwordsFromString($node->attr('alt'));
                }else{
                    $cleanElementsArray = $this->removeStopwordsFromString($node->text());
                }
                // generate a hashtag of a maximum 3 heading words
                $elementTextWords = "";
                $c = 1; $max = 3;
                foreach ($cleanElementsArray as $word) {
                    if(strlen($word) > 1){
                        if($c <= $max){
                            $elementTextWords .= str_replace("#", "", $word) . " "; // nop, not yet
                        }
                        $c++;
                    }
                }

                return rtrim($elementTextWords, " ");
            });
            if(count($filteredElements)){
                foreach ($filteredElements as $elementText) {
                    
                    $this->processings++;

                    if(strlen($elementText) > 2){
                        switch ($element) {
                            case 'h1': $weight = 40; break;
                            case 'h2': $weight = 30; break;
                            case 'a': $weight = 10; break;
                            case 'h3': $weight = 15; break;
                            case 'img': $weight = 2; break;
                            default: $weight = 1; break;
                        }
                        $this->hashtags[$this->currentIndex]['tag'] = $this->hashify($elementText);
                        $this->hashtags[$this->currentIndex]['weight'] = $weight; // --
                        $this->currentIndex++;
                    }

                    if($this->processings > $this->processingsLimit){
                        break;
                    }
                }
            }

            if($this->processings > $this->processingsLimit){
                break;
            }
        }


        /*
         * Count hashtags occurrences in our hashtags class variable
         */
        $totalHashtagsGenerated = count($this->hashtags);
   
        $hashtags = [];




        // calculate popularity just by how many occurances have
        //-- reorder array
        foreach ($this->hashtags as $key => $record) {

            if(array_key_exists($record['tag'], $hashtags)){
                // check weight then, store the big one
                if($record['weight'] > $hashtags[$record['tag']]['weight']){
                    $hashtags[$record['tag']]['weight'] = $record['weight'];
                }
                // increment occurances
                $hashtags[$record['tag']]['occurance']++;
            }else{
                $hashtags[$record['tag']]['weight'] = $record['weight'];
                $hashtags[$record['tag']]['occurance'] = 1;
            }
        }
        $index = 0;
        $totalUniqueHashtags = count($hashtags);
        foreach ($hashtags as $tagName => $record) {
            // score calculation:
            $hashtags[$tagName]['score'] = ($record['occurance'] * 100 / $totalUniqueHashtags) * $record['weight'];
        }

        // sort findings by score
        uasort($hashtags, function($a, $b) {
            // You can use an anonymous function like this for the uasort comparison function.
            if ($a['score'] > $b['score']) return -1;
            if ($a['score'] == $b['score']) return 0;
                // In case of 'score' ties, we may add a comparison of a different key
                return 1; // $a['score'] must be > $b['score'] at this point
        });
        
        // Finally, extract the first 50 hashtags
        $hashtags = array_slice($hashtags, 0, 50, true);
        
        return response()->json($hashtags);
  
        // return view('home', ['hashtags' => $hashtags]);

    }


    /**
     * Make a string hashtag like
     * 
     * @param  String
     * @return String
     */
    private function hashify($tag){
        return "#" . ucfirst(camel_case($tag));
    }

    /**
     * Removes special characters while maintaining spaces
     * 
     * @param  String
     * @return String
     */
    private function clearString($string){
        return trim(preg_replace('/[^A-Za-z0-9\- ]/', '', $string));
    }

    /**
     * Breaks string into an array of clean words
     * 
     * @param  String
     * @return String
     */
    private function breakString($string){
        // remove any special characters first, then explode string by space
        return explode(' ', $this->clearString($string));
    }

    /**
     * Removes stopwords from string
     * 
     * @param  String
     * @return Array
     */
    private function removeStopwordsFromString($string){
        $goodwordsArray = [];
        $wordsArray = $this->breakString($string);
        for($i = 0; $i < count($wordsArray); $i++){
            if(in_array($wordsArray[$i], $this->englishStopwords) || in_array($wordsArray[$i], $this->greekStopwords)){
                // bad description word
                continue;
            }else{
                $goodwordsArray[] = $this->hashify($wordsArray[$i]);
            }
        }
        return $goodwordsArray;
    }

    /**
     * Initialize stopwords containers (english & greek words only)
     * 
     * @return Void
     */
    private function initializeStopwords(){

        // initialize english stopwords
        $this->englishStopwords = ["a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the"];
        
        // initialize greek stopwords
        $this->greekStopwords = ["ο","η","το","οι","τα","του","της","των","τον","την","και","κι","κ","είμαι","είμαι","είσαι","είσαι","είναι","είναι","ειμαστε","είμαστε","είστε","στο","στον","στη","στην","μα","αλλα","αλλά","απο","από","για","προς","με","σε","ως","παρά","παρα","αντι","αντί","κατα","κατά","μετα","μετά","θα","να","δε","δεν","μη","μην","επι","ενω","εαν","αν","τοτε","τότε","που","πωσς","ποιος","ποια","ποιο","ποιοι","ποιες","ποιων","ποιους","αυτος","αυτός","αυτη","αυτή","αυτο","αυτό","αυτοι","αυτοί","αυτων","αυτών","αυτους","αυτούς","αυτες","αυτές","αυτα","αυτά","εκεινος","εκείνος","εκεινη","εκείνη","εκεινο","εκείνο","εκεινοι","εκείνοι","εκεινες","εκείνες","εκεινα","εκείνα","εκεινων","εκείνων","εκεινους","εκείνους","οπως","όπως","ομως","όμως","ισως","ίσως","οσο","όσο","οτι","ότι"];
    }
   
}
