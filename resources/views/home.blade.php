<!DOCTYPE html>
<html>
<head>
	<title>SemanticHashtags</title>
	<!-- Define Charset -->
	<meta charset="utf-8">
	<!-- Responsive Metatag -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	{{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
	<!-- Page Description and Author -->
	<meta name="description" content="">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
	<!-- Bootstrap CSS  -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/public/assets/plugins/bootstrap/v3.3.5/bootstrap.min.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/public/assets/plugins/bootstrap-material-design/css/bootstrap-material-design.min.css') }}" media="screen">
	<!-- Font Awesome CSS -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/public/assets/plugins/font-awesome/css/font-awesome.min.css') }}" media="screen">
	<!-- Main -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/public/assets/css/main.css') }}" media="screen">
</head>
<body>

	<!-- Header -->
	<header>

		<!-- Static navbar -->
		<nav class="navbar navbar-default">
	    	<div class="container-fluid">
	        	<div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
	            	<a class="navbar-brand pull-left" href=""> <div>#SemanticHashtags</div></a>
	            </div>
	            <div id="navbar" class="navbar-collapse collapse">
	            </div>
	            <!--/.nav-collapse -->
	        </div>
	    </nav>
	</header>

	<!-- Middle Column -->
	<div class='col-md-8 col-md-offset-2'>
		<div class="panel panel-default">
			<div class="panel-body">
				
				<form>
					<div class="form-group label-floating">
		                <label class="control-label" for="url"><i class="fa fa-link"></i> Valid Hyperlink</label>
		                <input id="url" type="text" required class="form-control" name="url" value="">
		                <a href="#" id="request-hashtags" data-action="{{ URL::route('ApiRoute') }}" class="btn btn-raised btn-warning pull-right" style="margin:5px auto;">Request</a>
		            </div>
				</form>

				<img class="spinner" src="{{ URL::asset('/public/assets/images/spinner.gif') }}" style="display:none; width: 100px; margin: auto;">

				<div id="hashtags">
				</div>

			</div>
		</div>
	</div>
	<!-- END Middle Column -->
	
	<!-- Start Footer -->
	<div class='footer'>
		<div class='row'>
			<div class='col-md-12 text-center'>
				<?= date('Y') ?> &copy; SemanticHashtags<br>Yiannis Despoti
			</div>
		</div>
	</div>
	<div class='space-40'></div>



</body>
</html>


<!-- JS  -->
<script type="text/javascript" src="{{ URL::asset('/public/assets/plugins/jQuery-lib/2.1.1/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/public/assets/plugins/jQuery-lib/2.1.1/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/public/assets/plugins/modernizrr-js/modernizrr.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/public/assets/plugins/bootstrap/v3.3.5/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/public/assets/plugins/bootstrap-material-design/js/material.min.js') }}"></script>
<!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="{{ URL::asset('/public/assets/plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/public/assets/js/main.js') }}"></script>