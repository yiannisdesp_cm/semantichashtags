// App Module
var app = (function (){

	// common methods on app init
	var process = {
		init: function(){

			$.material.init();

			// setup ajax with csrf token
			// $.ajaxSetup({
			//     headers: {
			//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			//     }
			// });
			
			// bind request btn
			$('#request-hashtags').click(function(){
				
				var apiRoute = $(this).data('action');
				var url = $('form #url').val();
				
				// display spinner
				$("img.spinner").show();

				$.ajax({
			        type: 'POST',
			        url: apiRoute,
			        dataType: 'JSON',
			        data : {url:url}
			    }).done(function (data) {
			    	var html = "";
			    	$.each(data, function(index, value){
			    		html += "<li><b>" + index + "</b> - Score: " + (value.score).toFixed(2) + "</li>";
			    	});
			        $("#hashtags").html(html);
			        // hide spinner
					$("img.spinner").hide();
			    }).fail(function () {
			        $("#hashtags").html('Invalid Request');
			        // hide spinner
					$("img.spinner").hide();
			    });
			});

			
		}
	};

	

	/* Return public methods */
	return {
		process:process
	}

})();

// Load App Module common methods
$(document).ready(function(){
	app.process.init();	
});
